package br.com.esocial;

import java.net.URL;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.util.AXIOMUtil;
import org.apache.commons.httpclient.protocol.Protocol;

import br.com.esocial.Util.SocketFactoryDinamico;
import br.gov.esocial.www.servicos.empregador.lote.eventos.envio.consulta.retornoprocessamento.v1_1_0.ServicoConsultarLoteEventosStub;
import jvCert.OnCert;

public class RecebeRetornoXml 
{
    private static final int SSL_PORT = 443;
    
    public static void main(String[] args) 
	{
        try 
        {					   
            URL url = new URL("https://webservices.producaorestrita.esocial.gov.br/servicos/empregador/consultarloteeventos/WsConsultarLoteEventos.svc");
            String[] strCertAlias = null;
			strCertAlias = OnCert.funcListaCertificados(false);
            OnCert.TAssinaXML tpAssinaXML = new OnCert.TAssinaXML();
            tpAssinaXML.strAliasTokenCert = strCertAlias[0];
            KeyStore ks = OnCert.funcKeyStore(strCertAlias[0]);
            String senhaDoCertificado = "93sa@01";
            String arquivoCacerts = "b:/eSocial/certificados/eSocialCacerts";
            String alias = "";
            Enumeration<String> aliasesEnum = ks.aliases();
            while (aliasesEnum.hasMoreElements()) 
            {
                alias = (String) aliasesEnum.nextElement();
                if (ks.isKeyEntry(alias)) 
                {
                    break;
                }
            }
            X509Certificate certificate = (X509Certificate) ks.getCertificate(alias);
            PrivateKey privateKey = (PrivateKey) ks.getKey(alias, senhaDoCertificado.toCharArray());
            SocketFactoryDinamico socketFactoryDinamico = new SocketFactoryDinamico(certificate, privateKey);
            socketFactoryDinamico.setFileCacerts(arquivoCacerts);

            Protocol protocol = new Protocol("https", socketFactoryDinamico, SSL_PORT);
            Protocol.registerProtocol("https", protocol);

    		StringBuilder sb = new StringBuilder();
    		sb.append("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
    		sb.append(" <soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"");
    		sb.append(" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"");
    		sb.append(" xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\">");
    		sb.append(" <soap:Header/>");
    		sb.append(" <soap:Body>");
//    		sb.append(" <eSocial xmlns=\"http://www.esocial.gov.br/schema/lote/eventos/envio/consulta/retornoProcessamento/v1_2_0\">");
    		sb.append(" <eSocial xmlns=\"http://www.esocial.gov.br/schema/lote/eventos/envio/consulta/retornoProcessamento/v1_0_0\">");
    		sb.append(" <consultaLoteEventos>");
    		sb.append(" <protocoloEnvio>1.2.201707.0000000000000081448</protocoloEnvio>");
    		sb.append(" </consultaLoteEventos>");
    		sb.append(" </eSocial>");

    		sb.append(" </soap:Body>");
    		sb.append(" </soap:Envelope>");
    		System.out.println("XML SOAP a ser enviado : "+sb.toString());
            
            OMElement ome = AXIOMUtil.stringToOM(sb.toString());
            ServicoConsultarLoteEventosStub.Consulta_type0 dadosMsgType0 = new ServicoConsultarLoteEventosStub.Consulta_type0();
            dadosMsgType0.setExtraElement(ome);

            ServicoConsultarLoteEventosStub.ConsultarLoteEventos distEnvioESocial = new ServicoConsultarLoteEventosStub.ConsultarLoteEventos();
            distEnvioESocial.setConsulta(dadosMsgType0);

            ServicoConsultarLoteEventosStub stub = new ServicoConsultarLoteEventosStub(url.toString());
            ServicoConsultarLoteEventosStub.ConsultarLoteEventosResponse result = stub.consultarLoteEventos(distEnvioESocial);  // neste momento � solicitada a senha do token
            result.getConsultarLoteEventosResult().getExtraElement().toString();

            System.out.println(result.getConsultarLoteEventosResult().getExtraElement().toString());
        } 
        catch (Exception ex) 
        {
            Logger.getLogger(EnvioXml.class.getName()).log(Level.SEVERE, null, ex);
        }
	}

}
