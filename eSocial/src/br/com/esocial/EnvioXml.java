/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.esocial;

import java.net.URL;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.util.AXIOMUtil;
import org.apache.commons.httpclient.protocol.Protocol;

import br.com.esocial.Util.SocketFactoryDinamico;
import br.gov.esocial.www.servicos.empregador.lote.eventos.envio.v1_1_0.ServicoEnviarLoteEventosStub;
import jvCert.OnCert;
import util.LerStream;

/**
 *
 * @author Usuario
 */
public class EnvioXml 
{
    private static final int SSL_PORT = 443;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) 
    {
        /*
         URL do Web Service de envio de lotes:
         • https://webservices.producaorestrita.esocial.gov.br/servicos/empregador/enviarloteeventos/WsEnviarLoteEventos.svc
         URL do Web Service de consulta de resultado de processamento de lotes:
         • https://webservices.producaorestrita.esocial.gov.br/servicos/empregador/consultarloteeventos/WsConsultarLoteEventos.svc 
         */
        try 
        {
            URL url = new URL("https://webservices.producaorestrita.esocial.gov.br/servicos/empregador/enviarloteeventos/WsEnviarLoteEventos.svc");
/*
 * 
 */
            String[] strCertAlias = null;
			strCertAlias = OnCert.funcListaCertificados(false);
            OnCert.TAssinaXML tpAssinaXML = new OnCert.TAssinaXML();
            tpAssinaXML.strAliasTokenCert = strCertAlias[0];
            KeyStore ks = OnCert.funcKeyStore(strCertAlias[0]);
/*
 *             
 */
//          String caminhoDoCertificadoDoCliente = "b:/eSocial/certificados/####.pfx";
            String senhaDoCertificado = "93sa@01";
            String arquivoCacerts = "b:/eSocial/certificados/eSocialCacerts";
/*
            InputStream entrada = new FileInputStream(caminhoDoCertificadoDoCliente);
            KeyStore ks = KeyStore.getInstance("pkcs12");
            try {
                ks.load(entrada, senhaDoCertificado.toCharArray());
            } catch (IOException e) {
                throw new Exception(
                        "Senha do Certificado Digital esta incorreta ou Certificado inválido.");
            }
*/
            String alias = "";
            Enumeration<String> aliasesEnum = ks.aliases();
            while (aliasesEnum.hasMoreElements()) 
            {
                alias = (String) aliasesEnum.nextElement();
                if (ks.isKeyEntry(alias)) 
                {
                    break;
                }
            }
            X509Certificate certificate = (X509Certificate) ks.getCertificate(alias);
            PrivateKey privateKey = (PrivateKey) ks.getKey(alias, senhaDoCertificado.toCharArray());
            SocketFactoryDinamico socketFactoryDinamico = new SocketFactoryDinamico(certificate, privateKey);
            socketFactoryDinamico.setFileCacerts(arquivoCacerts);

            Protocol protocol = new Protocol("https", socketFactoryDinamico, SSL_PORT);
            Protocol.registerProtocol("https", protocol);

            //String dados colocar o xml assinado
/*           
            String dados = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                    + "<eSocial xmlns=\"http://www.esocial.gov.br/schema/lote/eventos/envio/v1_1_0\">"
                    + "<!-- Xml do Evento -->"
                    + "<Signature xmlns=\"http://www.w3.org/2000/09/xmldsig#\">"
                    + "<SignedInfo>"
                    + "<CanonicalizationMethod Algorithm=\"http://www.w3.org/TR/2001/REC-xml-c14n-20010315\" />"
                    + "<SignatureMethod Algorithm=\"http://www.w3.org/2001/04/xmldsig-more#rsasha256\"/>"
                    + "<Reference URI=\"\">"
                    + "<Transforms>"
                    + "<Transform Algorithm=\"http://www.w3.org/2000/09/xmldsig#envelopedsignature\"/>"
                    + "<Transform Algorithm=\"http://www.w3.org/TR/2001/REC-xml-c14n-20010315\" />"
                    + "</Transforms>"
                    + "<DigestMethod Algorithm=\"http://www.w3.org/2001/04/xmlenc#sha256\" />"
                    + "<DigestValue>CFJEIy1dUko99nNUW/ICvG9ZNoij0o9IOhdP6Nt1j1k=</DigestValue>"
                    + "</Reference>"
                    + "</SignedInfo>"
                    + "<SignatureValue>...</SignatureValue>"
                    + "<KeyInfo>"
                    + "<X509Data>"
                    + "<X509Certificate>...</X509Certificate>"
                    + "</X509Data>"
                    + "</KeyInfo>"
                    + "</Signature>"
                    + "</eSocial>";

            OMElement ome = AXIOMUtil.stringToOM(dados);
*/
    		StringBuilder sb = new StringBuilder();
    		sb.append("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
    		sb.append("<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"");
    		sb.append(" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"");
    		sb.append(" xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\">");
    		sb.append(" <soap:Header/>");
    		sb.append(" <soap:Body>");
    		//
    		// Ler o lote composto de um ou mais XML�s a ser enviado ao ambiente do e-Social.
    		// Informar o caminho e o nome do XML 
    		String linha;
    		LerStream lerStream = new LerStream("n:\\eSocial\\AAESA\\aEnviar\\envioLoteEventos.xml");
    		if ( lerStream.prontoParaLeitura() )
    		{
    			while ((linha = lerStream.proximaLinha()) != "{EOF}") 
    			{
    				if (! linha.startsWith("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"))
    					sb.append(linha);
    			}
    			lerStream.close();
    		}
    		sb.append(" </soap:Body>");
    		sb.append(" </soap:Envelope>");
    		System.out.println("XML SOAP a ser enviado : "+sb.toString());
            
            OMElement ome = AXIOMUtil.stringToOM(sb.toString());
            ServicoEnviarLoteEventosStub.LoteEventos_type0 dadosMsgType0 = new ServicoEnviarLoteEventosStub.LoteEventos_type0();
            dadosMsgType0.setExtraElement(ome);

            ServicoEnviarLoteEventosStub.EnviarLoteEventos distEnvioEsocial = new ServicoEnviarLoteEventosStub.EnviarLoteEventos();
            distEnvioEsocial.setLoteEventos(dadosMsgType0);

            ServicoEnviarLoteEventosStub stub = new ServicoEnviarLoteEventosStub(url.toString());
            ServicoEnviarLoteEventosStub.EnviarLoteEventosResponse result = stub.enviarLoteEventos(distEnvioEsocial);  // neste momento � solicitada a senha do token
            result.getEnviarLoteEventosResult().getExtraElement().toString();

            System.out.println(result.getEnviarLoteEventosResult().getExtraElement().toString());
        } 
        catch (Exception ex) 
        {
            Logger.getLogger(EnvioXml.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
