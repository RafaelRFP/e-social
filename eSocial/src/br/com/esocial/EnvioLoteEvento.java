/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.esocial;

import java.io.File;
import java.net.URL;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.Enumeration;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.util.AXIOMUtil;
import org.apache.commons.httpclient.protocol.Protocol;

import br.com.esocial.Util.SocketFactoryDinamico;
import br.gov.esocial.www.servicos.empregador.lote.eventos.envio.consulta.retornoprocessamento.v1_1_0.ServicoConsultarLoteEventosStub;
import br.gov.esocial.www.servicos.empregador.lote.eventos.envio.v1_1_0.ServicoEnviarLoteEventosStub;
import jvCert.OnCert;
import util.ArquivosProperties;
import util.Aux_String;
import util.Diretorio;
/**
 * Num �nico processo, esta classe monta a mensagem <b><i>SOAP</i></b>, l� os <b>XML�s</b>, assina cada um deles, envelopa na mensagem, envia para o e-Social, 
 * recebe o recibo, monta mensagem SOAP para consultar o resultado do processamento e, finalmente, recebo o resultado do processamento do lote.
 *
 * @author Usuario
 * 
 * Modifica��es e/ou adapta��es feitas por pfugazza
 */
public class EnvioLoteEvento 
{
	private static final int SSL_PORT = 443;
	private static String urlEnviarLoteEventos = "https://webservices.producaorestrita.esocial.gov.br/servicos/empregador/enviarloteeventos/WsEnviarLoteEventos.svc";
	private static String urlConsultarLoteEventos = "https://webservices.producaorestrita.esocial.gov.br/servicos/empregador/consultarloteeventos/WsConsultarLoteEventos.svc";
	private static String schemaLoteEventosEnvio = "\"http://www.esocial.gov.br/schema/lote/eventos/envio/v1_1_1\"";
	private static String schemaLoteEventosEnvioConsultaRetornoProcessamento = "\"http://www.esocial.gov.br/schema/lote/eventos/envio/consulta/retornoProcessamento/v1_0_0\"";
	private static String protocoloEsocial = "";
	private static Properties properties;
	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) 
	{
		properties = ArquivosProperties.obterProperties("D:/JavaPSF/eclipseNeon/workspace/eSocial/src/eSocialScript.properties");
		/*
         URL do Web Service de envio de lotes:
         - https://webservices.producaorestrita.esocial.gov.br/servicos/empregador/enviarloteeventos/WsEnviarLoteEventos.svc
         URL do Web Service de consulta de resultado de processamento de lotes:
         - https://webservices.producaorestrita.esocial.gov.br/servicos/empregador/consultarloteeventos/WsConsultarLoteEventos.svc 
		 */
		try 
		{
			URL urlELE = new URL(urlEnviarLoteEventos);
			String[] strCertAlias = null;
			strCertAlias = OnCert.funcListaCertificados(false);
			OnCert.TAssinaXML tpAssinaXML = new OnCert.TAssinaXML();
			tpAssinaXML.strAliasTokenCert = strCertAlias[0];

			KeyStore ks = OnCert.funcKeyStore(strCertAlias[0]);
			String senhaDoCertificado = properties.getProperty("Senha");
			String arquivoCacerts = properties.getProperty("ArquivoDosCertificadosEsocial");

			String alias = "";
			Enumeration<String> aliasesEnum = ks.aliases();
			while (aliasesEnum.hasMoreElements()) 
			{
				alias = (String) aliasesEnum.nextElement();
				if (ks.isKeyEntry(alias)) 
				{
					break;
				}
			}
			X509Certificate certificate = (X509Certificate) ks.getCertificate(alias);
			PrivateKey privateKey = (PrivateKey) ks.getKey(alias, senhaDoCertificado.toCharArray());
			SocketFactoryDinamico socketFactoryDinamico = new SocketFactoryDinamico(certificate, privateKey);
			socketFactoryDinamico.setFileCacerts(arquivoCacerts);

			Protocol protocol = new Protocol("https", socketFactoryDinamico, SSL_PORT);
			Protocol.registerProtocol("https", protocol);

			// criar a mensagem SOAP

			StringBuilder sb = new StringBuilder();
			sb.append("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
			sb.append("<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"");
			sb.append(" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"");
			sb.append(" xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\">");
			sb.append(" <soap:Header/>");
			sb.append(" <soap:Body>");
			sb.append(" <eSocial xmlns="+schemaLoteEventosEnvio+">");
			sb.append(" <envioLoteEventos grupo=\"1\">");
			sb.append(" <ideEmpregador>");
			sb.append(" <tpInsc>1</tpInsc>");
			sb.append(" <nrInsc>"+properties.getProperty("CNPJ").substring(0,8)+"</nrInsc>");
			sb.append(" </ideEmpregador>");
			sb.append(" <ideTransmissor>");
			sb.append(" <tpInsc>1</tpInsc>");
			sb.append(" <nrInsc>"+properties.getProperty("CNPJ")+"</nrInsc>");
			sb.append(" </ideTransmissor>");
			sb.append(" <eventos>");
			//
			// aqui come�a o loop que ir� envelopar o(s) evento(s)
			//
			// Assinar o XML do evento. Informar caminho e arquivo de entrada e sa�da
			//
			Diretorio diretorio = new Diretorio(properties.getProperty("PastaDosXMLsAenviar"),new String[] {properties.getProperty("EventosAEnvelopar")});
			if (diretorio.quantosArquivosNaPasta() > 0)
			{
				for (File file : diretorio.getArquivosValidos())
				{
					tpAssinaXML.strArquivoXML = properties.getProperty("PastaDosXMLsAenviar")+file.getName();
					tpAssinaXML.strArquivoSaveXML = properties.getProperty("PastaDosXMLsAssinados")+file.getName().toUpperCase().replace(".XML", "")+"_signed.xml";
					OnCert.funcAssinaXML(tpAssinaXML);
					sb.append(" <evento "+tpAssinaXML.idEvento+">");   // este Id � o mesmo do Id do evento que foi assinado e ser� envelopado
					//
					// envelopar o XML assinado
					//
					sb.append(tpAssinaXML.xmlAssinado.replace("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>", ""));
					sb.append(" </evento>");
					//
					// aqui termina o loop que envelopou o(s) evento(s)
					//
				}
				sb.append(" </eventos>");
				sb.append(" </envioLoteEventos>");
				sb.append(" </eSocial>");

				sb.append(" </soap:Body>");
				sb.append(" </soap:Envelope>");
				System.out.println("XML SOAP a ser enviado : "+sb.toString());

				OMElement ome = AXIOMUtil.stringToOM(sb.toString());
				ServicoEnviarLoteEventosStub.LoteEventos_type0 dadosMsgType0 = new ServicoEnviarLoteEventosStub.LoteEventos_type0();
				dadosMsgType0.setExtraElement(ome);

				ServicoEnviarLoteEventosStub.EnviarLoteEventos distEnvioEsocial = new ServicoEnviarLoteEventosStub.EnviarLoteEventos();
				distEnvioEsocial.setLoteEventos(dadosMsgType0);

				ServicoEnviarLoteEventosStub stub = new ServicoEnviarLoteEventosStub(urlELE.toString());
				ServicoEnviarLoteEventosStub.EnviarLoteEventosResponse result = stub.enviarLoteEventos(distEnvioEsocial);  // neste momento � solicitada a senha do token
				result.getEnviarLoteEventosResult().getExtraElement().toString();

				System.out.println(result.getEnviarLoteEventosResult().getExtraElement().toString());
				protocoloEsocial = Aux_String.subStrIntoDelim(result.getEnviarLoteEventosResult().getExtraElement().toString(), "<protocoloEnvio>", "</protocoloEnvio>", true);
				System.out.println(protocoloEsocial);

				sb.setLength(0);
				sb.append("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
				sb.append(" <soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"");
				sb.append(" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"");
				sb.append(" xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\">");
				sb.append(" <soap:Header/>");
				sb.append(" <soap:Body>");
				sb.append(" <eSocial xmlns="+schemaLoteEventosEnvioConsultaRetornoProcessamento+">");
				sb.append(" <consultaLoteEventos>");
				sb.append(" <protocoloEnvio>"+protocoloEsocial+"</protocoloEnvio>");
				sb.append(" </consultaLoteEventos>");
				sb.append(" </eSocial>");

				sb.append(" </soap:Body>");
				sb.append(" </soap:Envelope>");
				System.out.println("XML SOAP a ser enviado : "+sb.toString());

				URL urlCLE = new URL(urlConsultarLoteEventos);
				OMElement omeCLE = AXIOMUtil.stringToOM(sb.toString());
				ServicoConsultarLoteEventosStub.Consulta_type0 dadosMsgTypeCLE = new ServicoConsultarLoteEventosStub.Consulta_type0();
				dadosMsgTypeCLE.setExtraElement(omeCLE);

				ServicoConsultarLoteEventosStub.ConsultarLoteEventos distEnvioESocial = new ServicoConsultarLoteEventosStub.ConsultarLoteEventos();
				distEnvioESocial.setConsulta(dadosMsgTypeCLE);

				ServicoConsultarLoteEventosStub stubCLE = new ServicoConsultarLoteEventosStub(urlCLE.toString());
				ServicoConsultarLoteEventosStub.ConsultarLoteEventosResponse resultCLE = stubCLE.consultarLoteEventos(distEnvioESocial); 
				resultCLE.getConsultarLoteEventosResult().getExtraElement().toString();

				System.out.println(resultCLE.getConsultarLoteEventosResult().getExtraElement().toString());
			}
			else
				System.out.println("N�o encontrado qualquer arquivo do tipo "+properties.getProperty("PastaDosXMLsAenviar")+properties.getProperty("EventosAEnvelopar"));
		} 
		catch (Exception ex) 
		{
			Logger.getLogger(EnvioXml.class.getName()).log(Level.SEVERE, null, ex);
		}

	}
}
