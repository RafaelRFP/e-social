/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.esocial.Util;
import javax.net.ssl.*;  
import java.io.*;  
import java.security.KeyStore;  
import java.security.MessageDigest;  
import java.security.cert.CertificateException;  
import java.security.cert.X509Certificate;  
  
/** 
* Gera o arquivo com os certificados de todos os servidores da NFE. 
* Baseado em http://www.javac.com.br/jc/posts/list/34.page 
*/  
public class EsocialBuildAllCacerts {  
  
    private static final String CACERTS_NAME = "eSocialCacerts";  
    private static final String CACERTS_PATH = "b:\\eSocial\\certificados";  
    private static final char SEPARATOR = File.separatorChar;  
    private static final int TIMEOUT_WS = 30;  
  
  
    public static void main(String[] args) {  
        try {  
  
            char[] passphrase = "changeit".toCharArray();  
            File file = new File(CACERTS_PATH + SEPARATOR + CACERTS_NAME);  
  
            if (file.isFile()) {  
                file.delete();  
            }  
  
            if (file.isFile() == false) {  
  
                File dir = new File(System.getProperty("java.home") + SEPARATOR + "lib" + SEPARATOR + "security");  
                file = new File(dir, CACERTS_NAME);  
                if (file.isFile() == false) {  
                    file = new File(dir, "cacerts");  
                }  
            }  
  
            info("| Loading KeyStore " + file + "...");  
            InputStream in = new FileInputStream(file);  
            KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());  
            ks.load(in, passphrase);  
            in.close();  
  
       
  
            //PRODUCAO  
            //AM - https://nfe.sefaz.am.gov.br  
            get("webservices.producaorestrita.esocial.gov.br", 443, ks);  
            
           
            System.out.println(CACERTS_PATH + SEPARATOR + CACERTS_NAME);  
            File cafile = new File(CACERTS_PATH + SEPARATOR + CACERTS_NAME);  
            OutputStream out = new FileOutputStream(cafile);  
            ks.store(out, passphrase);  
            out.close();  
        } catch (Exception e) {  
            e.printStackTrace();  
        }  
    }  
  
    public static void get(String host, int port, KeyStore ks) throws Exception {  
        SSLContext context = SSLContext.getInstance("TLS");  
        TrustManagerFactory tmf = TrustManagerFactory.getInstance(  
                TrustManagerFactory.getDefaultAlgorithm());  
        tmf.init(ks);  
        X509TrustManager defaultTrustManager = (X509TrustManager) tmf.getTrustManagers()[0];  
        SavingTrustManager tm = new SavingTrustManager(defaultTrustManager);  
        context.init(null, new TrustManager[]{tm}, null);  
        SSLSocketFactory factory = context.getSocketFactory();  
  
        info("| Opening connection to " + host + ":" + port + "...");  
        SSLSocket socket = (SSLSocket) factory.createSocket(host, port);  
        socket.setSoTimeout(TIMEOUT_WS * 1000);  
        try {  
            info("| Starting SSL handshake...");  
            socket.startHandshake();  
            socket.close();  
            info("| No errors, certificate is already trusted");  
        } catch (SSLHandshakeException e) {  
            /** 
             * PKIX path building failed: 
             * sun.security.provider.certpath.SunCertPathBuilderException: 
             * unable to find valid certification path to requested target 
             * N�o tratado, pois sempre ocorre essa exceo quando o cacerts 
             * nao esta gerado. 
             */  
        } catch (SSLException e) {  
            error("| " + e.toString());  
        }  
  
        X509Certificate[] chain = tm.chain;  
        if (chain == null) {  
            info("| Could not obtain server certificate chain");  
        } else {  
            info("| Server sent " + chain.length + " certificate(s):");  
            MessageDigest sha1 = MessageDigest.getInstance("SHA1");  
            MessageDigest md5 = MessageDigest.getInstance("MD5");  
            for (int i = 0; i < chain.length; i++) {  
                X509Certificate cert = chain[i];  
                sha1.update(cert.getEncoded());  
                md5.update(cert.getEncoded());  
  
                String alias = host + "-" + (i);  
                ks.setCertificateEntry(alias, cert);  
                info("| Added certificate to keystore '" + CACERTS_PATH + SEPARATOR + CACERTS_NAME + "' using alias '" + alias + "'");  
            }  
        }  
    }  
  
    private static class SavingTrustManager implements X509TrustManager {  
        private final X509TrustManager tm;  
        private X509Certificate[] chain;  
  
        SavingTrustManager(X509TrustManager tm) {  
            this.tm = tm;  
        }  
  
        @Override  
        public X509Certificate[] getAcceptedIssuers() {  
            return new X509Certificate[0];  
            // throw new UnsupportedOperationException();  
        }  
  
        @Override  
        public void checkClientTrusted(X509Certificate[] chain, String authType)  
                throws CertificateException {  
            throw new UnsupportedOperationException();  
        }  
  
        @Override  
        public void checkServerTrusted(X509Certificate[] chain, String authType)  
                throws CertificateException {  
            this.chain = chain;  
            this.tm.checkServerTrusted(chain, authType);  
        }  
    }  
  
  
    private static void info(String log) {  
        System.out.println("INFO: " + log);  
    }  
  
    private static void error(String log) {  
        System.out.println("ERROR: " + log);  
    }  
  
}  
