package testes;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;

public class TestSOAP_3
{
	private static SOAPMessage criarMensagem() throws Exception
	{
/*		
		<soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:byjg="urn:http://www.byjg.com.br">
		   <soapenv:Header/>
		   <soapenv:Body>
		      <byjg:obterLogradouroAuth soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
			    <cep xsi:type="xsd:string">88304430</cep>
		         <usuario xsi:type="xsd:string">pfugazza</usuario>
		         <senha xsi:type="xsd:string">3l1r1aQF</senha>
		      </byjg:obterLogradouroAuth>
		   </soapenv:Body>
		</soapenv:Envelope>
*/
		StringBuilder sb = new StringBuilder();
		sb.append("<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:byjg=\"urn:http://www.byjg.com.br\">");
		sb.append(" <soapenv:Header/>");
		sb.append(" <soapenv:Body>");
		sb.append(" <byjg:obterLogradouroAuth soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">");
		sb.append(" <cep xsi:type=\"xsd:string\">21051545</cep>");
		sb.append(" <usuario xsi:type=\"xsd:string\">usuarioAqui</usuario>");
		sb.append(" <senha xsi:type=\"xsd:string\">senhaAqui</senha>");
		sb.append(" </byjg:obterLogradouroAuth>");
		sb.append(" </soapenv:Body>");
		sb.append(" </soapenv:Envelope>");
		InputStream is = new ByteArrayInputStream(sb.toString().getBytes());
//		SOAPMessage soapMessage = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL).createMessage(null, is);
		SOAPMessage soapMessage = MessageFactory.newInstance().createMessage(null, is);

		System.out.println("Request SOAP Message = ");
		soapMessage.writeTo(System.out);
		System.out.println();

		return soapMessage;
	}
	/**
	 * Method used to create the SOAP Request
	 */
	private static SOAPMessage createSOAPRequest() throws Exception
	{
		MessageFactory messageFactory = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);
//		MessageFactory messageFactory = MessageFactory.newInstance();
		SOAPMessage soapMessage = messageFactory.createMessage();
		SOAPPart soapPart = soapMessage.getSOAPPart();
		String nameSpace = "psf";

		// SOAP Envelope
		SOAPEnvelope envelope = soapPart.getEnvelope();
		envelope.addNamespaceDeclaration(nameSpace, "http://www.byjg.com.br/site/webservice.php/ws/cep");
		// SOAP Body
		SOAPBody soapBody = envelope.getBody();
		
		SOAPElement soapBodyElem = soapBody.addChildElement("obterLogradouroAuth", nameSpace);
		SOAPElement soapBodyElem1 = soapBodyElem.addChildElement("cep", nameSpace);
		soapBodyElem1.addTextNode("88304430");
		SOAPElement soapBodyElem2 = soapBodyElem.addChildElement("usuario", nameSpace);
		soapBodyElem2.addTextNode("usuarioAqui");
		SOAPElement soapBodyElem3 = soapBodyElem.addChildElement("senha", nameSpace);
		soapBodyElem3.addTextNode("senhaAqui");

		soapMessage.saveChanges();

		// Check the input
		System.out.println("Request SOAP Message = ");
		soapMessage.writeTo(System.out);
		System.out.println();
		return soapMessage;
	}

	/**
	 * Method used to print the SOAP Response
	 */
	private static void printSOAPResponse(SOAPMessage soapResponse) throws Exception
	{
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		Source sourceContent = soapResponse.getSOAPPart().getContent();
		System.out.println("\nResponse SOAP Message = ");
		StreamResult result = new StreamResult(System.out);
		transformer.transform(sourceContent, result);
	}

	/**
	 * Starting point for the SAAJ - SOAP Client Testing
	 */
	public static void main(String args[])
	{
		try
		{
			// Create SOAP Connection
			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory.createConnection();
			
			//Send SOAP Message to SOAP Server
			String url = "http://www.byjg.com.br/site/webservice.php/ws/cep"; //?WSDL";
			SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(), url);
//			SOAPMessage soapResponse = soapConnection.call(criarMensagem(), url);

			// Process the SOAP Response
			printSOAPResponse(soapResponse);
//			soapResponse.writeTo(System.out);
//			System.out.println();

			soapConnection.close();
		}
		catch (Exception e)
		{
			System.err.println("Error occurred while sending SOAP Request to Server");
			e.printStackTrace();
		}
	}

}
