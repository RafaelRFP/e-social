package testes;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;

import util.LerStream;

public class TestEsocial
{
	private static SOAPMessage criarMensagem() throws Exception
	{
		StringBuilder sb = new StringBuilder();
		sb.append("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
		sb.append("<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"");
		sb.append(" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"");
		sb.append(" xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\">");
		sb.append(" <soap:Header/>");
		sb.append(" <soap:Body>");
		//
		// Ler o lote de XML a ser enviado ao ambiente do e-Social
		String linha;
		LerStream lerStream = new LerStream("b:\\xml\\envioLoteEventos_signed.xml");
		if ( lerStream.prontoParaLeitura() )
		{
			while ((linha = lerStream.proximaLinha()) != "{EOF}") 
			{
				sb.append(linha);
			}
			lerStream.close();
		}
		//
		sb.append(" </soap:Body>");
		sb.append(" </soap:Envelope>");
		InputStream is = new ByteArrayInputStream(sb.toString().getBytes());
		SOAPMessage soapMessage = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL).createMessage(null, is);
/*
		SOAPPart soapPart = soapMessage.getSOAPPart();
		SOAPEnvelope envelope = soapPart.getEnvelope();
		String nameSpace = "";
		envelope.addNamespaceDeclaration(nameSpace, "webservices.esocial.gov.br");
*/
		System.out.println("Mensagem SOAP de requisição = ");
		soapMessage.writeTo(System.out);
		System.out.println();

		return soapMessage;
	}
	/**
	 * Method used to create the SOAP Request
	 */
	private static SOAPMessage createSOAPRequest() throws Exception
	{
		MessageFactory messageFactory = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL);
		SOAPMessage soapMessage = messageFactory.createMessage();
		SOAPPart soapPart = soapMessage.getSOAPPart();
		String nameSpace = "ns";

		// SOAP Envelope
		SOAPEnvelope envelope = soapPart.getEnvelope();
		envelope.addNamespaceDeclaration(nameSpace, "webservices.esocial.gov.br");
		// SOAP Body
		SOAPBody soapBody = envelope.getBody();
		
		SOAPElement soapBodyElem = soapBody.addChildElement("obterEnderecoDesteCEP", nameSpace);
		SOAPElement soapBodyElem1 = soapBodyElem.addChildElement("cep", nameSpace);
		soapBodyElem1.addTextNode("21051545");

		soapMessage.saveChanges();

		// Check the input
		System.out.println("Mensage SOAP de resposta = ");
		soapMessage.writeTo(System.out);
		System.out.println();
		return soapMessage;
	}

	/**
	 * Method used to print the SOAP Response
	 */
	private static void printSOAPResponse(SOAPMessage soapResponse) throws Exception
	{
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		Source sourceContent = soapResponse.getSOAPPart().getContent();
		System.out.println("\nMensagem SAOP de resposta = ");
		StreamResult result = new StreamResult(System.out);
		transformer.transform(sourceContent, result);
	}

	/**
	 * Starting point for the SAAJ - SOAP Client Testing
	 */
	public static void main(String args[])
	{
        String[] strCertAlias = null;
		try
		{
			// Create SOAP Connection
			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory.createConnection();

			//Send SOAP Message to SOAP Server
			String url = "https://webservices.producaorestrita.esocial.gov.br/servicos/empregador/enviarloteeventos/WsEnviarLoteEventos.svc";
//			SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(), url);
			SOAPMessage soapResponse = soapConnection.call(criarMensagem(), url);

			// Process the SOAP Response
			printSOAPResponse(soapResponse);

			soapConnection.close();
		}
		catch (Exception e)
		{
			System.err.println("Error occurred while sending SOAP Request to Server");
			e.printStackTrace();
		}
	}

}
