package testes;
import java.net.MalformedURLException;
import java.net.URL;

import java.security.Security;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPConstants;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.Element;

public class Soap 
{
	public static Node transmitir(URL endpoint, Document cabecDoc, Document dadosDoc, String soapAction) throws Exception 
	{
		Node elemento = null;
		Node adotado = null;

		System.setProperty("java.protocol.handler.pkgs", "com.sun.net.ssl.internal.www.protocol");
		Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());

		System.setProperty("javax.net.ssl.keyStoreType", "PKCS12");
		System.setProperty("javax.net.ssl.keyStore", "D:\\Nfe\\certificado.pfx");
		System.setProperty("javax.net.ssl.keyStorePassword", "senha");

		SOAPMessage reqSoap = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL).createMessage();

		reqSoap.getSOAPPart().setXmlStandalone(true);
		reqSoap.getMimeHeaders().addHeader("SOAPAction", soapAction);

		elemento = cabecDoc.getDocumentElement();
		adotado = reqSoap.getSOAPPart().adoptNode(elemento);
		reqSoap.getSOAPHeader().appendChild(adotado);

		elemento = dadosDoc.getDocumentElement();
		adotado = reqSoap.getSOAPPart().adoptNode(elemento);
		reqSoap.getSOAPBody().appendChild(adotado);


		SOAPConnectionFactory cnnFactory = SOAPConnectionFactory.newInstance();
		SOAPConnection cnn = cnnFactory.createConnection();
		SOAPMessage respSoap = cnn.call(reqSoap, endpoint);

		Node nodo = respSoap.getSOAPBody().getChildNodes().item(0);

		return nodo;
	}

	public static void main(String[] args) 
	{
		URL endpoint2 = null;
		try 
		{
			endpoint2 = new URL("https://homologacao.nfe.ms.gov.br/homologacao/services2/NfeStatusServico2");
		} 
		catch (MalformedURLException ex) 
		{
			Logger.getLogger(Soap.class.getName()).log(Level.SEVERE, null, ex);
		}
		Document Doc = null;
		Document Doc2 = null;
		try 
		{
			Doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
			Doc2 = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
		} 
		catch (ParserConfigurationException ex) 
		{
			Logger.getLogger(Soap.class.getName()).log(Level.SEVERE, null, ex);
		}
		// Doc
		Element nfeCabecMsg = Doc.createElement("nfeCabecMsg");
		nfeCabecMsg.setAttribute("xmlns", "http://www.portalfiscal.inf.br/nfe/wsdl/NfeStatusServico2");

		Element versaoDados = Doc.createElement("versaoDados");
		versaoDados.setTextContent("2.00");
		nfeCabecMsg.appendChild(versaoDados);

		Element cUF = Doc.createElement("cUF");
		cUF.setTextContent("51");
		nfeCabecMsg.appendChild(cUF);

		Doc.appendChild(nfeCabecMsg);

		Element nfeDadosMsg = Doc2.createElement("nfeDadosMsg");
		nfeCabecMsg.setAttribute("xmlns", "http://www.portalfiscal.inf.br/nfe/wsdl/NfeStatusServico2");

		Element consStatServ = Doc2.createElement("consStatServ");
		consStatServ.setAttribute("xmlns", "http://www.portalfiscal.inf.br/nfe");
		consStatServ.setAttribute("versao", "2.00");

		Element tpAmb = Doc2.createElement("tpAmb");
		tpAmb.setTextContent("2");
		consStatServ.appendChild(tpAmb);

		Element cUF2 = Doc2.createElement("cUF");
		cUF2.setTextContent("2");
		consStatServ.appendChild(cUF2);

		Element xServ = Doc2.createElement("xServ");
		xServ.setTextContent("STATUS");
		consStatServ.appendChild(xServ);

		nfeDadosMsg.appendChild(consStatServ);

		Doc2.appendChild(nfeDadosMsg);

		String soapAction2 = "http://www.portalfiscal.inf.br/nfe/wsdl/NfeStatusServico2/nfeStatusServicoNF2";

		try 
		{
			JOptionPane.showMessageDialog(null, transmitir(endpoint2, Doc, Doc2, soapAction2).toString());
		} 
		catch (Exception ex) 
		{
			Logger.getLogger(Soap.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

}