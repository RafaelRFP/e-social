package testes;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.MessageDigest;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;

public class TestSOAP_2
{
	private static SOAPMessage criarMensagem() throws Exception
	{
		StringBuilder sb = new StringBuilder();
		sb.append("<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\"");
		sb.append(" xmlns:ns=\"http://wsDiversos.araujoabreu.com.br\">");
		sb.append(" <soap:Header/>");
		sb.append(" <soap:Body>");

		sb.append(" <ns:validarLogin>");
		sb.append(" <ns:loginName>admin</ns:loginName>");

		MessageDigest md = MessageDigest.getInstance("MD5");  
		BigInteger hash = new BigInteger(1, md.digest("senhaAqui".getBytes()));   
		sb.append(" <ns:password>"+hash.toString(16)+"</ns:password>");
		sb.append(" </ns:validarLogin>");
/*		
		sb.append(" <ns:obterEnderecoDesteCEP>");
		sb.append(" <ns:cep>20771450</ns:cep>");
		sb.append(" </ns:obterEnderecoDesteCEP>");
*/		
		sb.append(" </soap:Body>");
		sb.append(" </soap:Envelope>");
		InputStream is = new ByteArrayInputStream(sb.toString().getBytes());
		SOAPMessage soapMessage = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL).createMessage(null, is);

		System.out.println("Request SOAP Message = ");
		soapMessage.writeTo(System.out);
		System.out.println();

		return soapMessage;
	}
	/**
	 * Method used to create the SOAP Request
	 */
	private static SOAPMessage createSOAPRequest() throws Exception
	{
		MessageFactory messageFactory = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL);
		SOAPMessage soapMessage = messageFactory.createMessage();
		SOAPPart soapPart = soapMessage.getSOAPPart();
		String nameSpace = "ns";

		// SOAP Envelope
		SOAPEnvelope envelope = soapPart.getEnvelope();
		envelope.addNamespaceDeclaration(nameSpace, "http://wsDiversos.araujoabreu.com.br");
		// SOAP Body
		SOAPBody soapBody = envelope.getBody();
		
		SOAPElement soapBodyElem = soapBody.addChildElement("obterEnderecoDesteCEP", nameSpace);
		SOAPElement soapBodyElem1 = soapBodyElem.addChildElement("cep", nameSpace);
		soapBodyElem1.addTextNode("21051545");

/*		
		SOAPElement soapBodyElem = soapBody.addChildElement("validarLogin", nameSpace);
		SOAPElement soapBodyElem1 = soapBodyElem.addChildElement("loginName", nameSpace);
		soapBodyElem1.addTextNode("admin");
		SOAPElement soapBodyElem2 = soapBodyElem.addChildElement("password", nameSpace);
		MessageDigest md = MessageDigest.getInstance("MD5");  
		BigInteger hash = new BigInteger(1, md.digest("senhaAqui".getBytes()));   
		soapBodyElem2.addTextNode(hash.toString(16));
*/		
/*		
		SOAPElement soapBodyElem = soapBody.addChildElement("pesquisarPorCpf", nameSpace);
		SOAPElement soapBodyElem1 = soapBodyElem.addChildElement("cpf", nameSpace);
		soapBodyElem1.addTextNode("01793610746");
*/		
		soapMessage.saveChanges();

		// Check the input
		System.out.println("Request SOAP Message = ");
		soapMessage.writeTo(System.out);
		System.out.println();
		return soapMessage;
	}

	/**
	 * Method used to print the SOAP Response
	 */
	private static void printSOAPResponse(SOAPMessage soapResponse) throws Exception
	{
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		Source sourceContent = soapResponse.getSOAPPart().getContent();
		System.out.println("\nResponse SOAP Message = ");
		StreamResult result = new StreamResult(System.out);
		transformer.transform(sourceContent, result);
	}

	/**
	 * Starting point for the SAAJ - SOAP Client Testing
	 */
	public static void main(String args[])
	{
		try
		{
			// Create SOAP Connection
			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory.createConnection();

			//Send SOAP Message to SOAP Server
			String url = "http://200.223.212.42:8080/axis2/services/Diversos?wsdl";
			SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(), url);
//			SOAPMessage soapResponse = soapConnection.call(criarMensagem(), url);

			// Process the SOAP Response
			printSOAPResponse(soapResponse);

			soapConnection.close();
		}
		catch (Exception e)
		{
			System.err.println("Error occurred while sending SOAP Request to Server");
			e.printStackTrace();
		}
	}

}
