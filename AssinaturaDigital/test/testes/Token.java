package testes;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.SecureRandom;
import java.util.Enumeration;
import javax.xml.bind.DatatypeConverter;
/*
 * Retirado em 20/07/2017 de https://gist.github.com/JuniorPolegato/10d22a722d9051b51b87
 */
class Token {
	public static void main(String[] a) 
	{
		System.out.print("Mensagem: ");
		String message = System.console().readLine();
		char[] pin = null;
		KeyStore ks = null;
		String alias = "";

		try 
		{
			while (alias == "") 
			{
				System.out.print("Insira o cart�o e pressione [Enter]...");
				System.console().readPassword();
				try 
				{
					ks = KeyStore.getInstance("PKCS11");
					System.out.print("Senha: ");
					pin = System.console().readPassword();
					ks.load(null, pin);
					System.out.println("Logado!");
					Enumeration<String> aliases = ks.aliases();
					alias = (String)aliases.nextElement();
				} 
				catch (Exception e) 
				{
					System.out.print("Erro: ");
					System.out.println(e);
				}
			}

			System.out.print("Assinando a mensagem com: ");
			System.out.println(alias);

			PrivateKey signerKey = (PrivateKey) ks.getKey(alias, null);
			Signature signature = Signature.getInstance("SHA1withRSA", ks.getProvider());

			signature.initSign(signerKey, new SecureRandom());
			signature.update(message.getBytes());
			byte[] sigBytes = signature.sign();

			System.out.print("Assinatura: ");
			System.out.println(DatatypeConverter.printHexBinary(sigBytes));

			ks = null;
		} 
		catch(Exception nfe) {
			System.out.println("Faiou!");
			System.out.println(nfe);
		} 
		finally 
		{
			System.out.println("Fim!");
			System.out.println("Aguarde o logout ou remova o cart�o...");
		}
	}
}