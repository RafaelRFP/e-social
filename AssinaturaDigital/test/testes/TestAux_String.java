package testes;

import util.Aux_String;

public class TestAux_String 
{
	private static String texto = "<eSocial xmlns=\"http://www.esocial.gov.br/schema/lote/eventos/envio/retornoEnvio/v1_1_0\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><retornoEnvioLoteEventos><ideEmpregador><tpInsc>1</tpInsc><nrInsc>33373325</nrInsc></ideEmpregador><ideTransmissor><tpInsc>1</tpInsc><nrInsc>33373325000179</nrInsc></ideTransmissor><status><cdResposta>201</cdResposta><descResposta>Lote Recebido com Sucesso.</descResposta></status><dadosRecepcaoLote><dhRecepcao>2017-08-02T13:32:45.554663-03:00</dhRecepcao><versaoAplicativoRecepcao>0.1.0-A0200</versaoAplicativoRecepcao><protocoloEnvio>1.2.201708.0000000000000094978</protocoloEnvio></dadosRecepcaoLote></retornoEnvioLoteEventos></eSocial>";
	private static String textoParte = "</versaoAplicativoRecepcao><protocoloEnvio>1.2.201708.0000000000000094978</protocoloEnvio></dadosRecepcaoLote></retornoEnvioLoteEventos></eSocial>";

	public static void main(String[] args) 
	{
		System.out.println(Aux_String.subStrIntoDelim(texto, "<protocoloEnvio>", "</protocoloEnvioN", true));
		System.out.println(Aux_String.subStrIntoDelim("<evtInfoEmpregador Id=\"ID1333733250000002017072816231100001\">", "<", " ",true));
		System.out.println(Aux_String.subStrIntoDelim("<evtInfoEmpregador Id=\"ID1333733250000002017072816231100001\">", " ", ">",true));
		System.out.println(Aux_String.subStrIntoDelim("Paulo.Sergio.Fugazza", "lo.", ".Fu", true));
		System.out.println(Aux_String.subStrIntoDelim("Paulo.Sergio.Fugazza", "lo.", ".Fu", false));
	}

}
