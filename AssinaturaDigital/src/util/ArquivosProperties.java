package util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 * Leitura de arquivos <i>.properties</i>
 * @author pfugazza
 *
 */
public class ArquivosProperties 
{
	public static Properties obterProperties(String qualArquivo) 
	{
		Properties properties = new Properties();
		try
		{
			properties.load(new FileInputStream(qualArquivo));
		}
		catch (NullPointerException e1)
		{
			Logger.getLogger(ArquivosProperties.class.getName()).log(Level.SEVERE, qualArquivo+" n�o pode ser lido.", e1);
		} 
		catch (FileNotFoundException e1) 
		{
			Logger.getLogger(ArquivosProperties.class.getName()).log(Level.SEVERE, qualArquivo+" n�o encontrado.", e1);
		} 
		catch (IOException e1) 
		{
			Logger.getLogger(ArquivosProperties.class.getName()).log(Level.SEVERE, qualArquivo+" n�o foi lido.", e1);
		}
		return properties;
	}
}