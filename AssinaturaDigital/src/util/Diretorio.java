package util;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
/**
 * Classe para tratamento de diret�rio
 * @author pfugazza
 */
public class Diretorio
{
	private String pasta;
	private File folder;
	private List<File> arquivosValidos = new ArrayList<File>();
	/**
	 * Ler diret�rio desta pasta
	 * @param pasta
	 */
	public Diretorio(String pasta)
	{
		this.pasta = pasta;
		folder = new File(pasta);
	}
	/**
	 * Obter da <i>pasta</i> todos os arquivos que atendam a pelo menos uma das <i>mascaras</i>.
	 * @param pasta
	 * @param mascaras
	 * @see pastaExiste() para saber se pasta � v�lida.
	 * @see getArquivosValidos() para obter a lista de arquivos.
	 */
	public Diretorio(String pasta, String[] mascaras)
	{
		this.pasta = pasta;
		folder = new File(pasta);
		if (pastaExiste())
		{
			arquivosValidos.clear();
			quaisOsArquivos(pasta, mascaras);
		}
	}
	/**
	 * Criar uma hierarquia de pastas
	 * @param qualServico Por ora, o �nico servi�o � <b>CriarPastas</b>
	 * @param drive Drive onde criar a hierarquia de pastas
	 * @param pastas Pastas que ser�o criadas. Ir� criar as pastas at� encontrar o �ltimo caracter <b>/</b>.</br>
	 * Exemplo: </br>
	 * Diretorio diretorio = new Diretorio("CriarPastas","b:", "Geral/sc/exe/Custos.exe")
	 */
	public Diretorio(String qualServico, String drive, String pastas) 
	{
		if (pastas.lastIndexOf("/") > -1)
		{
			folder = new File(drive+"\\"+pastas.substring(0, pastas.lastIndexOf("/")));
			folder.mkdirs();
		}
	}

	public boolean pastaExiste()
	{
		return folder.exists();
	}
	/**
	 * Verifica a quantidade de arquivos nesta pasta
	 * @return
	 */
	public int quantosArquivosNaPasta()
	{
		return folder.list().length;
	}
	/*	
	protected void quaisArquivos(final String mascara) 
	{
		FilenameFilter textFilter = new FilenameFilter() 
		{
			public boolean accept(File dir, String name) 
			{
				return (Aux_String.compara(name,mascara));
			}
		};
		filesDeAcordoComMascara = folder.listFiles(textFilter);
	}
	 */		
	/**
	 * Verifica quais arquivos que combinam com a mascara.
	 * @param mascara no formato ??????abc.pdf
	 */
	/*	
	public File[] obterArquivosComEstaMascara(String mascara)
	{
		quaisArquivos(mascara);
		return filesDeAcordoComMascara;
	}
	 */	
	/**
	 * Obter a quantidade de arquivos que combinam com a mascara.
	 * @param mascara no formato ??????abc.pdf
	 */
	/*	
	public int quantosArquivosComEstaMascara(String mascara)
	{
		quaisArquivos(mascara);
		return filesDeAcordoComMascara.length;
	}
	 */	
	public String pastaRaizDestaProcura()
	{
		return this.pasta;
	}
	/**
	 * Pesquisar pelos arquivos a partir da pasta corrente que coincidam com a(s) mascara(s).</br>
	 * Exemplos de m�scara : *.RTF, *.TXT, ????_MFP.*  
	 * @param mascaras <i>array</i> de <i>String</i> 
	 * @return lista de <i>File</i> que foram encontrados na pasta corrente e suas sub-pastas
	 */
	/*	
	@SuppressWarnings("unchecked")
	public List<File> quaisOsArquivos(String[] mascaras)
	{
		return (List<File>) FileUtils.listFiles(folder, new WildcardFileFilter(mascaras), TrueFileFilter.TRUE);
	}
	 */	
	/**
	 * Rotina copiada de http://stackoverflow.com/questions/1384947/java-find-txt-files-in-specified-folder
	 * em 27/01/2015.<p>
	 * Pesquisar pelos arquivos a partir da pasta raiz que coincidam com a(s) mascara(s).</br>
	 * Exemplos de m�scara : *.RTF, *.TXT, ????_MFP.*  
	 * @param mascaras <i>array</i> de <i>String</i> 
	 * @return lista de <i>File</i> que foram encontrados na pasta corrente e suas sub-pastas
	 */
	/*	
	public List<File> quaisOsArquivos(String[] mascaras)
    {
        List<File> directories = new ArrayList<>();
        directories.add(folder);

        List<File> textFiles = new ArrayList<>();

        FileFilter typeFilter = new WildcardFileFilter(mascaras);

        while (directories.isEmpty() == false)
        {
            List<File> subDirectories = new ArrayList();
            for(File f : directories)
            {
                subDirectories.addAll(Arrays.asList(f.listFiles((FileFilter)DirectoryFileFilter.INSTANCE)));
                textFiles.addAll(Arrays.asList(f.listFiles(typeFilter)));
            }
            directories.clear();
            directories.addAll(subDirectories);
        }
        return textFiles;
    }
	 */
	/**
	 * C�digo copiado de http://stackoverflow.com/questions/2056221/recursively-list-files-in-java </br>
	 * no dia 30/01/2015<p>
	 * Pesquisar pelos arquivos a partir da pasta raiz <i/>path</i> que coincidam com pelo menos uma da(s) </br>
	 * mascara(s) e os guarda numa <i>List</i> de <i>File</i> que poder� ser devolvida via <i>getArquivosValidos()</i>.</br>
	 * Exemplos de m�scara : 3635_?FP, 3635????.???, ????_MFP
	 * @param path pasta raiz da pesquisa  
	 * @param mascaras <i>array</i> de <i>String</i> 
	 * @see getArquivosValidos()
	 */
	private void quaisOsArquivos( String path, String[] mascaras ) 
	{
		File root = new File( path );
		File[] list = root.listFiles();

		for ( File f : list ) 
		{
			if ( f.isDirectory() ) 
			{
				quaisOsArquivos( f.getAbsolutePath(),mascaras );
			}
			else 
			{
				if (Aux_String.compara(f.getName(),mascaras))
					arquivosValidos.add(f);
			}
		}
	}
	/**
	 * Obter a lista de arquivos
	 * @return
	 */
	public List<File> getArquivosValidos() {
		return arquivosValidos;
	}

}
